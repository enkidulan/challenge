import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.rst')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'docopt',
    'aiohttp',
    'cchardet',
    'aiodns',
    'venusian',
    'lxml',
    'colander',
    'zope.interface',
    'zope.component',
]

tests_require = [
    'pytest',
    'pytest-cov',
    'testfixtures',
    'pylint',
    'cssselect',
]

setup(
    name='challenge',
    version='0.0',
    description='challenge',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        'Programming Language :: Python',
    ],
    author='',
    author_email='',
    url='',
    keywords='python',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    extras_require={
        'testing': tests_require,
    },
    install_requires=requires,
    entry_points={
        'console_scripts': [
            'challenge_update = challenge:cli',
        ],
    },
)
