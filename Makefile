.DEFAULT_GOAL := build

PYTHON?=python3
VENV?=parts/venv
ENV_TYPE?=development
ENV_OPTINOS?=--system-site-packages


ensure-venv:
	@echo '============================= creating virtual environmetn ============================='
	[ -d $(VENV) ] || { virtualenv $(ENV_OPTINOS) -p $(PYTHON) $(VENV);}


build-initial-dependencies:  ensure-venv
	@echo '============================ installing initial dependencies ==========================='
	$(VENV)/bin/pip install --upgrade pip setuptools wheel
	$(VENV)/bin/pip install --upgrade zc.buildout


build: build-initial-dependencies
	@echo '============================ installing initial dependencies ==========================='
	[ -f buildout.cfg ] || { cp buildout.cfg.example  buildout.cfg;}
	@sed -i -e 's/# \+profiles\/$(ENV_TYPE).cfg/    profiles\/$(ENV_TYPE).cfg/'  buildout.cfg
	$(VENV)/bin/buildout


test: build
	@echo '================================ running project tests ================================='
	[ -f bin/test ] || @make build
	bin/test

clean:
	@rm -rf $(VENV)
	@rm -rf __pycache__
	@rm -rf .cache
	@rm -rf develop-eggs
	@rm -rf parts
	@rm -rf bin
	@rm -rf *.egg-info
	@rm -rf eggs
	@rm -rf .coveragerc
	@rm -rf .installed.cfg
