************
Requirements
************

Challenge is to write a BOT, which can extract odds/probability for all the horse in
King George VI winner (Win/EW) market from links give below and generate
a real-time matrix from it. Code should either print matrix to screen or save it in
file, which should update at given interval (by default every 5 minutes).
For example the matrix should have following format.

         +--------+-------+-------+-------+-------+-------+-------+-------+
         |        | site1 | site2 | site3 | site4 | site5 | site6 | site7 |
         +========+=======+=======+=======+=======+=======+=======+=======+
         | Horse1 |       |       |       |       |       |       |       |
         +--------+-------+-------+-------+-------+-------+-------+-------+
         | Horse2 |       |       |       |       |       |       |       |
         +--------+-------+-------+-------+-------+-------+-------+-------+
         | Horse3 |       |       |       |       |       |       |       |
         +--------+-------+-------+-------+-------+-------+-------+-------+
         | Horse4 |       |       |       |       |       |       |       |
         +--------+-------+-------+-------+-------+-------+-------+-------+
         | Horse5 |       |       |       |       |       |       |       |
         +--------+-------+-------+-------+-------+-------+-------+-------+
         | Horse6 |       |       |       |       |       |       |       |
         +--------+-------+-------+-------+-------+-------+-------+-------+
         | Horse7 |       |       |       |       |       |       |       |
         +--------+-------+-------+-------+-------+-------+-------+-------+

Points will be given based on:

    1. Code correctness
    2. Code design and architecture
    3. Speed of execution

List of web pages to extract odds from are as follows:

    1. http://sports.williamhill.com/bet/en-gb/betting/e/11029917/ap/Ascot+%2d+King+George+VI+Stakes+%2d+29th+Jul+2017.html
    2. https://www.skybet.com/horse-racing/ascot/event/20693785
    3. http://www.paddypower.com/racing/future-racing/king-george-vi--chase
    4. http://www.paddypower.com/racing/future-racing/king-george-vi--chase
    5. https://www.bet365.com/?lng=1&cb=105812028182#/AC/B2/C172/D101/E33281960/F2/G-1/H-1/P10/


********
Solution
********

Time limit: I don't spent more than half of day on working on challenge solutions,
so realization of this solution is a bit different form what I have in mind when
I've been writing this spec.


Design decisions
================

Goals:
    * Componentization
    * Low cohesion
    * Scalability and speed
    * Resilience
    * Traceability

Componentization
----------------

Solution defines Interfaces of its components and uses component register. Configurator
allows to build system from custom pieces. Those pieces provides corresponding
interfaces, it allows to replace any of system components configuration after
its initialization, all components are referred through component register. Each component
provides general interface that allows it to be independent and been able to be run in
a separate environment including dedicated servers.

Scalability and speed
---------------------

**Scalability means speed on a large scale.** Solution is based on service-pipe architecture,
where each component cares only about its data-source and result storage endpoints.
Those endpoints are queue alike objects that provides a message bus between components,
which allows to scale components independently and dynamically.

**Speed on a small scale.** Fetcher is I/O bound component, it separates slow out-world
from fast internal messaging bus by using asyncio as a cheap and efficient solution to deal with I/O waits.
DataMiner is CPU bound component, it makes sense to scale this component across different CPUs/servers,
as part of solution it uses a multiprocessing workers.

Another speed related consideration is that it could be nice to use a selenium because
it provides features that xml parses is not capable of or it's more complicated to do by using xml
parser, but comparatively selenium it's veeery slow, so pass on it.

Resilience
----------

Using css or xpath selectors to get the data from page is not very resilient and won't work across
multiple sites. As far as we know how represented on the page data looks like,
it's more efficient to extract data based on structure analysis, and regexp_lucky_guess_extractor
that mixes dom-tree inspection with reg-exp based content recognition does it!

Traceability
------------

All issues during data extracting are sent to anomalies storage, including: not 200 response code,
no data or data validation errors.


*****
Usage
*****

Targeted platforms are linux based distributions, sorry no sys deps list, to install run:

.. code-block:: bash

    make

This after it cronjob will be added to crontab that will run every 5 minutes. Execution log will
appear in `run.log` file in project root.

File `sites.txt` contains a list of urls to fetch, results will appear in `out.csv` file,
hard-codding sucks, but well, I'm out of time.

To user manually run:

.. code-block:: bash

    bin/challenge_update


**********
After word
**********

A lot of things are missing to make this solution ready to use. But well its a good thing for
a demo, after all I've spent on it more than half of a day but less than a day...\


Solution detects some abnormalities for some links, including:
    * no data for 'url': 'https://www.bet365.com/?lng=1&amp;cb=105812028182#/AC/B2/C172/D101/E33281960/F2/G-1/H-1/P10/'
    * wrong response code for 'url': 'https://www.skybet.com/horse-racing/ascot/event/20693785', 'status': 403

I also have questions about data consistency and whole process over all that should be communicated
with "product owner", but that would be too much for a demo.
