from collections import defaultdict
import csv
from multiprocessing import Queue

from zope import interface
from zope import component

from .interfaces import IStorage, IDataMiner
from .typing import Config, Entry


@interface.implementer(IStorage)
class CSVStorage:

    def __init__(self, config: Config):
        self.config = config
        self.queue = Queue()

    def put(self, entry: Entry) -> None:
        self.queue.put(entry)

    def save(self) -> None:
        component.getUtility(IDataMiner).join()  # Making sure that all tasks in DataMiner is done

        results = []
        while not self.queue.empty():
            results.append(self.queue.get())

        hosres_odds_per_site = defaultdict(dict)

        for site_data in results:
            host = site_data['host']
            for horse_stat in site_data['extracted_data']:
                horse = horse_stat['horse']
                odds = horse_stat['odds']
                hosres_odds_per_site[horse][host] = odds

        all_hosts = [i['host'] for i in results]

        plain_stats = []
        for horse, horse_hosts in hosres_odds_per_site.items():
            hosts_stats = {'horse': horse}
            for host in all_hosts:
                hosts_stats[host] = horse_hosts.get(host, None)
            plain_stats.append(hosts_stats)

        csv_out_fields = ['horse'] + all_hosts
        with open('out.csv', 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_out_fields)
            writer.writeheader()
            writer.writerows(plain_stats)
