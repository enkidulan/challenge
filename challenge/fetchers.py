import asyncio

import aiohttp
import async_timeout
from zope import component
from zope import interface

from .interfaces import IFetcher, IDataSource, IDataMiner, IAnomalyStorage
from .typing import Config


@interface.implementer(IFetcher)
class AsyncFetcher:

    def __init__(self, config: Config) -> None:
        self.config = config
        self.data_source = component.getUtility(IDataSource)
        self.data_miner = component.getUtility(IDataMiner)

    def fetch(self) -> None:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(fetch_all(self.data_source, self.data_miner))


async def fetch(session: aiohttp.ClientSession, url: str, data_miner: IDataMiner) -> None:
    with async_timeout.timeout(10):
        async with session.get(url) as response:
            text = await response.text()
    entry = {
        'host': response.host.replace('www.', '', 1),
        'url': url,
        'status': response.status,
        'body': text}
    (data_miner if response.status == 200 else component.getUtility(IAnomalyStorage)).put(
        entry)


async def fetch_all(data_source: IDataSource, data_miner: IDataMiner) -> None:
    tasks = []

    async with aiohttp.ClientSession() as session:
        for url in data_source.get_links():
            # TODO: add limit - no more than 1000 per session, things may get ugly
            #       otherwise in case or teardown/error
            task = asyncio.ensure_future(fetch(session, url, data_miner))
            tasks.append(task)
        await asyncio.wait(tasks)
