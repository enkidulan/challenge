from zope import component
from .interfaces import IDataSource, IAnomalyStorage, IDataMiner, IFetcher, IStorage


def configure(config):

    from .source_providers import FileDataSource
    component.provideUtility(FileDataSource(config), IDataSource)

    from .anomalies_storages import StreamStorage
    component.provideUtility(StreamStorage(config), IAnomalyStorage)

    from .storages import CSVStorage
    component.provideUtility(CSVStorage(config), IStorage)

    from .data_miners import MultiprocessingMiner
    component.provideUtility(MultiprocessingMiner(config), IDataMiner)

    from .fetchers import AsyncFetcher
    component.provideUtility(AsyncFetcher(config), IFetcher)


def challenge(config=None, configurator=configure):
    config = {} if config is None else config
    configure(config)
    component.getUtility(IFetcher).fetch()
    component.getUtility(IStorage).save()


def cli():
    challenge()
