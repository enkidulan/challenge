from io import StringIO
import os
import re
from multiprocessing import Process, JoinableQueue, Queue

import colander
import lxml.etree
from zope import interface
from zope import component

from .interfaces import IDataMiner, IAnomalyStorage, IDataExtractor, IStorage


from .typing import Iterable, Config, Entry, JsonAble, SRE_Match, LXML_Node


class HorseOdds(colander.MappingSchema):

    horse = colander.SchemaNode(colander.String())
    odds = colander.SchemaNode(colander.String(), validator=colander.Regex('^\d{1,2}/\d{1,2}$'))


class HorsesOdds(colander.SequenceSchema):
    horse_odds = HorseOdds()


# NOTE: In real project would use aws lambda or similar for extracting data form page
@interface.implementer(IDataMiner)
class MultiprocessingMiner:

    def __init__(self, config: Config, schema: colander.SchemaNode=HorsesOdds()) -> None:
        self.config = config
        self.queue = JoinableQueue()
        self.schema = schema
        self.result_backed = component.getUtility(IStorage)
        self.anomalies_backend = component.getUtility(IAnomalyStorage)
        processes = [
            Process(target=worker,
                    args=(self.queue, self.schema, self.result_backed, self.anomalies_backend),
                    daemon=True)
            for i in range(os.cpu_count())
        ]
        for p in processes:
            p.start()

    def put(self, entry: Entry) -> None:
        self.queue.put(entry)

    def join(self) -> None:
        self.queue.join()


def worker(
        queue: Queue,
        schema: colander.SchemaNode,
        result_backed: IStorage,
        anomalies_backend: IAnomalyStorage) -> None:
    while True:
        entry = queue.get()

        data = extract_data(entry)
        msg = {
            'extracted_data': data,
            'host': entry['host'],
        }
        backing = result_backed

        deserialize_errors = None
        try:
            schema.deserialize(data)
        except colander.Invalid as errors:
            deserialize_errors = errors.asdict()

        if deserialize_errors or not data:
            msg.update(entry)
            msg['deserialize_errors'] = deserialize_errors
            backing = anomalies_backend

        backing.put(msg)
        queue.task_done()


def extract_data(entry: Entry) -> JsonAble:
    data_extractor = component.queryUtility(
        IDataExtractor, entry['host'], default=regexp_lucky_guess_extractor)
    data = data_extractor(entry)
    return data


def fast_test(line: str) -> bool:
    return bool(re.search('\d{1,2}/\d{1,2}', line))


def is_a_bottom_test(line: str) -> bool:
    return bool(re.search('^\d{1,2}/\d{1,2}$', line))


def extract(line: str) -> SRE_Match:
    return re.search('(?P<horse>[A-Za-z ]+).+ (?P<odds>\d{1,2}/\d{1,2})', line)


def clean_text(text: str) -> str:
    text = text.replace('View Tip', '')  # XXX: seems like hidden text, need to take css in consideration
    text = text.strip()
    return text


def textify(node: LXML_Node) -> str:
    try:
        return ' '.join((clean_text(i) for i in node.itertext() if clean_text(i)))
    except Exception:
        return ''


def get_nodes(node: LXML_Node) -> Iterable[LXML_Node]:
    nodes = []
    for elem in node.iterchildren():
        text = textify(elem)
        if not fast_test(text):
            continue
        if is_a_bottom_test(text):
            nodes.append(node)
        for child in get_nodes(elem):
            nodes.append(child)
    return nodes


def regexp_lucky_guess_extractor(entry: Entry) -> Iterable[dict]:
    parser = lxml.etree.HTMLParser(recover=True)
    dom = lxml.etree.parse(StringIO(entry['body']), parser=parser).getroot()
    nodes = get_nodes(dom)
    rez = []
    for node in nodes:
        match = extract(textify(node)) or extract(textify(node.getparent()))
        if match is None:
            continue
        rez.append(match.groupdict())
    return rez
