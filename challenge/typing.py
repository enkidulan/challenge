from typing import Any, Iterable, Dict


SRE_Match = Any
LXML_Node = Any
JsonAble = Any
Config = Dict[str, str]
Entry = Dict[str, JsonAble]
