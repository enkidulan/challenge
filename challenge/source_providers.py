from zope import interface
from .interfaces import IDataSource
from .typing import Config, Iterable


@interface.implementer(IDataSource)
class FileDataSource:

    def __init__(self, config: Config) -> None:
        self.config = config
        # XXX: get form config
        config['source_file'] = 'sites.txt'

    def get_links(self) -> Iterable[str]:
        links = set(l.strip() for l in open('sites.txt').readlines())
        for link in links:
            yield link
