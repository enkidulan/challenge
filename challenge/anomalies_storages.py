import sys

from zope import interface
from .interfaces import IDataSource

from .typing import Config, Entry


@interface.implementer(IDataSource)
class StreamStorage:

    def __init__(self, config: Config) -> None:
        self.config = config

    def put(self, entry: Entry) -> None:
        sys.stdout.write(str(entry))
