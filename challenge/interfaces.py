from zope import interface


class IComponent(interface.Interface):
    "Interface for links provider"

    config = interface.Attribute("Application config")


class IBus(interface.Interface):
    "Bus provider"

    def put(entry):
        "adds new entry to anomalies storage"


class IDataSource(IComponent):
    "Interface for links provider"

    def get_links():
        "return generator of links, one link at a time"


class IAnomalyStorage(IBus, IComponent):
    "Interface for gathering errors and anomalies during data extracting"


class IDataMiner(IBus, IComponent):
    "Interface for data mining provider"

    def join():
        "Makes sure that all items are proceeded"


class IFetcher(IComponent):
    "Interface for fetching"

    def fetch():
        "Runs job on fetching all link"


class IDataExtractor(IComponent):
    "Interface for extracting data from page"


class IStorage(IBus, IComponent):
    "Interface for provider to store final results"

    def save():
        "ensures all jobs are done and saves results"
